package com.example.examen03;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;


@Entity(tableName = "journal_table")
public class Journal {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "title")
    private String title;

    @NonNull
    @ColumnInfo(name = "content")
    private String content;

    @NonNull
    @ColumnInfo(name = "published_on")
    private Date published_on;


    /*public Journal(int id, String title, String content,Date published_on)
    {
        this.id = id;
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }*/


    public Journal(String title, String content,Date published_on)
    {
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    public void setId(int id){this.id=id;}

    public void setTitleetTitle(String title){this.title = title;}

    public void setContent(String content){this.content = content;}

    public void setPublished_on(Date published_on){this.published_on = published_on;}

    public int getId(){return this.id;}

    public String getTitle(){return this.title;}

    public String getContent(){return this.content;}

    public Date getPublished_on(){return this.published_on;}



}
