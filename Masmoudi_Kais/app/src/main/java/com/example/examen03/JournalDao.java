package com.example.examen03;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface JournalDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Journal journal);

    @Delete
    void deleteJournal(Journal journal);

    @Update
    void updateJournal(Journal journal);

    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Query("SELECT * from journal_table ORDER BY id ASC")
    LiveData<List<Journal>> getAllJournals();
}
