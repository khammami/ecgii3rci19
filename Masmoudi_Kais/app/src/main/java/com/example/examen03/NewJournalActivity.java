package com.example.examen03;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

public class NewJournalActivity extends AppCompatActivity {

    private EditText mEditTitleView;
    private EditText mEditDateView;
    private EditText mEditContentView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_journal);

        mEditTitleView = findViewById(R.id.edit_title);
        mEditDateView = findViewById(R.id.edit_date);
        mEditContentView = findViewById(R.id.edit_content);

        mEditDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickDate(view);
            }
        });

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if ( (TextUtils.isEmpty(mEditTitleView.getText())) || (TextUtils.isEmpty(mEditContentView.getText())) )
                {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String title = mEditTitleView.getText().toString();
                    Date date = calendar.getTime();
                    String content = mEditContentView.getText().toString();
                    replyIntent.putExtra("Title", title);
                    replyIntent.putExtra("Date", date);
                    replyIntent.putExtra("Content", content);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });

    }

    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    public void pickDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(NewJournalActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                            mEditDateView.setText((day + "/" + (month+1) + "/" + year));

                    }
                }, year, month, dayOfMonth);
        datePickerDialog.show();


    }

}
