package com.example.examen_gii;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class Note_tableViewModel {
    private TableRepository mRepository;

    private LiveData<List<Note_table>> mAllNote_table;

    public Note_tableViewModel (Application application) {
        super(application);
        mRepository = new TableRepository(application);
        mAllNote_table = mRepository.getAllWords();
    }

    LiveData<List<Note_table>> getAllWords() { return mAllNote_table; }

    public void insert(Note_table word) { mRepository.insert(word); }
}
