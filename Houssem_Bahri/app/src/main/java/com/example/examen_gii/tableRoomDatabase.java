package com.example.examen_gii;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Note_table.class}, version = 1, exportSchema = false)
public abstract class tableRoomDatabase extends RoomDatabase {
    public abstract Note_tableDao wordDao();
    private static tableRoomDatabase INSTANCE;

    static tableRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (tableRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            tableRoomDatabase.class, "Note_table_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    public abstract Note_tableDao Note_tableDao();
}
