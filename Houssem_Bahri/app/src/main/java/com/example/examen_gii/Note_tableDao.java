package com.example.examen_gii;

        import androidx.lifecycle.LiveData;
        import androidx.room.Dao;
        import androidx.room.Insert;
        import androidx.room.Query;
        import androidx.room.Delete;

        import java.util.List;

@Dao
public interface Note_tableDao {




    @Insert
    void insert(Note_table Note_table);

    @Query("DELETE FROM Note_table")
    void deleteAll();

    @Query("SELECT * from Note_table ORDER BY Note_table DESC")
    List<Note_table> getAllWords();
}

