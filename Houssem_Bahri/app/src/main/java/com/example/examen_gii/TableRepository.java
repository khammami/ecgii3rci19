package com.example.examen_gii;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class TableRepository {
    private Note_tableDao mNote_tableDao;
    private LiveData<List<Note_table>> mAllNote_table;

    TableRepository(Application application) {
        tableRoomDatabase db = tableRoomDatabase.getDatabase(application);
        mNote_tableDao  = db.Note_tableDao();

    }

    LiveData<List<Note_table>> getAllWords() {
        return mAllNote_table;
    }

    public void insert (Note_table Note_table) {
        new insertAsyncTask(mNote_tableDao).execute(Note_table);
    }

    private static class insertAsyncTask extends AsyncTask<Note_table, Void, Void> {

        private Note_tableDao mAsyncTaskDao;

        insertAsyncTask(Note_tableDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Note_table... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
