package com.example.examen_gii;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class Note_tableListAdapter<ViewHolder, RecyclerView> {
    private final LayoutInflater mInflater;
    private List<Note_table> title; // Cached copy of words

    Note_tableListAdapter(Context context) {
        LayoutInflater mInflater1;
        mInflater1 = LayoutInflater.from(context);
        mInflater = mInflater1;
    }

    public Note_tableListAdapter(LayoutInflater mInflater) {
        this.mInflater = mInflater;
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new noteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        if (title != null) {
            Note_table current = title.get(position);
            holder.noteItemView.setText(current.getTitle());
        } else {
            // Covers the case of data not being ready yet.
            holder.noteItemView.setText("No Word");
        }
    }

    void setWords(List<Note_table> words){
        title = title;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (title != null)
            return title.size();
        else return 0;
    }
    RecyclerView
    class WordViewHolder extends .ViewHolder {
        private final TextView noteItemView;

        private View itemView;
        WordViewHolder(View itemView) {
            super(itemView);
            noteItemView = itemView.findViewById(R.id.textView);
        }
    }
}
