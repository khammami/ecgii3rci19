package com.example.examentp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {

    private final LayoutInflater mInflater;
    private List<Word> mNotes; // Cached copy of words

    NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new NoteViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        if (mNotes != null) {
            Word current = mNotes.get(position);
            holder.wordItemView1.setText(current.getTitle());
            holder.wordItemView2.setText(current.getTitle());
            holder.wordItemView2.setText(current.getTitle());

        } else {
            // Covers the case of data not being ready yet.
            holder.wordItemView1.setText("No table");
            holder.wordItemView2.setText("No table");
            holder.wordItemView3.setText("No table");

        }



    }

    void setWords(List<Word> words){
        mNotes = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mNotes != null)
            return mNotes.size();
        else return 0;
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {
        private final TextView wordItemView1;
        private final TextView wordItemView2;
        private final TextView wordItemView3;

        private NoteViewHolder(View itemView) {
            super(itemView);
            wordItemView1 = itemView.findViewById(R.id.textView);
            wordItemView2 = itemView.findViewById(R.id.Date);
            wordItemView3 = itemView.findViewById(R.id.content);
        }
    }


        }



