package com.example.noteapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "note_table")
public class Note {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int mId;
    @ColumnInfo(name = "title")
    private String mTitle;
    @ColumnInfo(name = "content")
    private String mContent;
    @ColumnInfo(name = "published_on")
    private Date mPublished_on;

    public Note(@NonNull int id, String title, String content, Date published_on) {
        this.mId = id;
        this.mTitle = title;
        this.mContent = content;
        this.mPublished_on = published_on;
    }

    public int getmId() {
        return this.mId;
    }

    public String getmTitle() {
        return this.mTitle;
    }

    public String getmContent() {
        return this.mContent;
    }

    public Date getmPublished_on() {
        return this.mPublished_on;
    }
}
