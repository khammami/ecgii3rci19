package com.example.roomdate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_DATE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            data data = new data(data.getStringExtra(NewDate.EXTRA_REPLY));
            Dataviewmodel.insert(data);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
    public static final int NEW_DATE_ACTIVITY_REQUEST_CODE = 1;
}
