package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
@Dao
public interface JournalDao {


    @Insert
    void insert(MainActivity.Journal journal);

    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Query("SELECT * from journal_table ORDER BY published_on ASC")
    LiveData<List<MainActivity.Journal> > getAllWords();

}