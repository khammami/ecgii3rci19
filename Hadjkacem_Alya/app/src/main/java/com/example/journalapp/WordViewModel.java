package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WordViewModel extends AndroidViewModel {

    private WordRepository mRepository;

    private LiveData<List<MainActivity.Journal>> mAllWords;

    public WordViewModel (Application application) {
        super(application);
        mRepository = new WordRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<MainActivity.Journal>> getAllWords() { return mAllWords; }

    public void insert(MainActivity.Journal journal) { mRepository.insert(journal); }
}