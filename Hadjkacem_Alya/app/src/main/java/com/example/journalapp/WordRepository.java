package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WordRepository {

    private JournalDao mJournalDao;
    private LiveData<List<MainActivity.Journal>> mAllWords;

    WordRepository(Application application) {
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        mJournalDao = db.wordDao();
        mAllWords = mJournalDao.getAllWords();
    }

    LiveData<List<MainActivity.Journal>> getAllWords() {
        return mAllWords;
    }

    public void insert (MainActivity.Journal journal) {
        new insertAsyncTask(mJournalDao).execute(journal);
    }

    private static class insertAsyncTask extends AsyncTask<MainActivity.Journal, Void, Void> implements com.example.journalapp.insertAsyncTask {

        private JournalDao mAsyncTaskDao;

        insertAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MainActivity.Journal... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }

        public void execute(MainActivity.Journal journal) {
        }
    }
}