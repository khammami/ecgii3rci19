package com.example.examen;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.examen.Note;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Query("DELETE FROM note_table")
    void deleteAll();

    @Query("SELECT * from note_table ORDER BY published_on ASC")
    LiveData<List<Note>> getAllWords();

    @Query("DELETE FROM note_table")
    void delete(Note note);

    @Update
    void update(Note... note);


}