package com.example.examen;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.examen.Note;
import com.example.examen.NoteRepository;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {

    private NoteRepository mRepository;

    private LiveData<List<Note>> mAllWords;

    public NoteViewModel (Application application) {
        super(application);
        mRepository = new NoteRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<Note>> getAllWords() { return mAllWords; }

    public void insert(Note note) { mRepository.insert(note); }
    public void deleteAll() {mRepository.deleteAll();}
    public void deleteWord(Note note) {
        mRepository.deleteWord(note);
    }

    public void update(Note note) {
        mRepository.update(note);
    }

}