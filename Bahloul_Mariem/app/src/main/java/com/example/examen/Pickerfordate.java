package com.example.examen;


import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.app.DatePickerDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pickerfordate extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {


    public Pickerfordate() {
        // Required empty public constructor
    }

    public void onDateSet(DatePicker datePicker,
                          int year, int month, int day){
        Two activity = (Two) getActivity();
        Calendar c = Calendar.getInstance();
        c.set(year,month,day);
        activity.setDate(c.getTime());}

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the current date as the default date in the picker.
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        // Create a new instance of DatePickerDialog and return it.
        return new DatePickerDialog(getActivity(), this, year, month, day);

    }
}
