package com.example.journalapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
@Database(entities = {data.class}, version = 1, exportSchema = false)

public abstract class DateRoomDataBase extends RoomDatabase {

    public abstract DataDao DataDao();
    private static DateRoomDataBase INSTANCE;

    public static DateRoomDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DateRoomDataBase.class) {
                if (INSTANCE == null) {
                    // Create database here

                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DateRoomDataBase.class, "Date_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
