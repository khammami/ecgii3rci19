package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class DateViewModel  extends AndroidViewModel {

    private DateRepository mRepository;

    private LiveData<List<data>> mAllWords;

    public DateViewModel (Application application) {
        super(application);
        mRepository = new DateRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<data>> getAllWords() { return mAllWords; }

    public void insert(data data) { mRepository.insert(data); }
}
