package com.example.journalapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import androidx.room.TypeConverters;

@Entity(tableName = "journal_table")
public class data {

    @NonNull
    @ColumnInfo(name = "data")
    @PrimaryKey(autoGenerate=true)
    private int id;
    private String mdata;

    public data(@NonNull String data)
    {this.mdata = data;}

    public String getdata()
    {return this.mdata;}

    public String title()
    {return this.mdata;}

    public String content()
    {return this.mdata;}




}
