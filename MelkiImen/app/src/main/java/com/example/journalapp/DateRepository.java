package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class DateRepository {
    private DataDao mdataDao;
    private LiveData<List<data>> mAllDate;
    DateRepository(Application application) {
        DateRoomDataBase db = DateRoomDataBase.getDatabase(application);
        mdataDao = db.DataDao();
        mAllDate = mdataDao.getAlldata();
    }

    LiveData<List<data>> getAllWords() {
        return mAllDate;
    }

    public void insert (data data) {
        new insertAsyncTask(mdataDao).execute(data);
    }

    private static class insertAsyncTask extends AsyncTask<data, Void, Void> {

        private DataDao mAsyncTaskDao;

        insertAsyncTask(DataDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final data... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}
