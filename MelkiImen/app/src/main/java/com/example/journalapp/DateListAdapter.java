package com.example.journalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.List;

public class DateListAdapter extends RecyclerView.Adapter<DateListAdapter.DateViewHolder> {

    private final LayoutInflater mInflater;
    private List<data> mdata; // Cached copy of words

    DateListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public DateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recylerview, parent, false);
        return new DateViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DateViewHolder holder, int position) {
        if (mdata != null) {
            data current = mdata.get(position);
            holder.dataItemView.setText(current.getdata());
        } else {
            // Covers the case of data not being ready yet.
            holder.dataItemView.setText("No date");
        }
    }

    void setData(List<data> data) {
        mdata = data;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mdata != null)
            return mdata.size();
        else return 0;
    }

    class DateViewHolder extends RecyclerView.ViewHolder {
        private final TextView dataItemView;

        private DateViewHolder(View itemView) {
            super(itemView);
            dataItemView = itemView.findViewById(R.id.textView);
        }
    }
}
