package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DataDao {
    @Insert
    void insert(data data);

    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Query("SELECT * from journal_table ORDER BY data ASC")
    List<data> getAlldata();

}
