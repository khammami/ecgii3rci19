package com.example.exam;

import android.content.Intent;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class NewWordActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY =
            "com.example.android.exam.REPLY";

    private EditText mEditTitleView;
    private EditText mEditContentView;
    private TextView mDateView;
    private Button mButtonDate;
    private Date mdate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mEditTitleView = findViewById(R.id.editTitle);
        mEditContentView = findViewById(R.id.editContent);
        mDateView = findViewById(R.id.textViewDate);


        final Button button = findViewById(R.id.buttonDate);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(),
                        "Button");
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.newactivitymenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            Intent replyIntent = new Intent();
            if (TextUtils.isEmpty(mEditTitleView.getText()) || TextUtils.isEmpty(mEditContentView.getText())) {
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                String title = mEditTitleView.getText().toString();
                String content = mEditContentView.getText().toString();
                String date = mDateView.getText().toString();
                replyIntent.putExtra(EXTRA_REPLY, new String[]{title, content, date});
                setResult(RESULT_OK, replyIntent);
            }
            finish();

        }

        return super.onOptionsItemSelected(item);
    }
    public void processDatePickerResult(int year, int month, int day) {
        mdate = new Date(day,month,year);
        mDateView.setText(Converter.dateToTimestamp(mdate).toString());
    }
}
