package com.example.exam;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "note_table")
public class Word {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "content")
    private String content;
    @ColumnInfo(name = "published_on")
    private Date published_on;

    public Word(@NonNull int id, @NonNull String title,@NonNull String content,@NonNull Date published_on)
    {
        this.id=id;
        this.content = content;
        this.title=title;
        this.published_on=published_on;
    }



    public int getId(){return this.id;}
    public String getTitle(){return this.title;}
    public String getContent(){return this.content;}
    public Date getPublished_on(){return this.published_on;}
}
