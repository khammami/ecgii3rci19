package com.example.examen;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "note_table")
public class Note {
    @PrimaryKey(autoGenerate=true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;
    @NonNull
    @ColumnInfo(name = "title")
    private String title;
    @NonNull
    @ColumnInfo(name = "content")
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @ColumnInfo(name = "published_on")
    private Date published_on;
    @Ignore
    public Note(@NonNull String title) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    public Note(@NonNull String title, @NonNull String content, @NonNull Date published_on) {
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    @NonNull
    public Date getPublished_on() {
        return published_on;
    }

    public void setPublished_on(@NonNull Date published_on) {
        this.published_on = published_on;
    }


}
