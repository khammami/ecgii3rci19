package com.example.examen;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {
    private NoteRepository mRepository;
    private LiveData<List<Note>> mAllNotes;


    public NoteViewModel(Application application) {
        super(application);
        mRepository = new NoteRepository(application);
        mAllNotes = mRepository.getmAllNotes();
    }

    public LiveData<List<Note>> getmAllNotes() {
        return mAllNotes;
    }
    public void insert(Note note) { mRepository.insert(note); }
    public void deleteAll() {mRepository.deleteAll();}
    public void deleteWord(Note note) {mRepository.deleteNote(note);}
    public void update(Note note) {
        mRepository.update(note);
    }

}
