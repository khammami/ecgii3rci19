package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.journalapp.MainActivity;

import java.util.List;

public interface JournalDao {
    @Insert
    void insert(MainActivity.Journal word);
    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Query("SELECT * from journal_table ORDER BY published_on DESC")
    LiveData<List<MainActivity.Journal>> getAllWords();
}
