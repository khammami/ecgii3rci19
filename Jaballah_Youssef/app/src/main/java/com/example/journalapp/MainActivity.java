package com.example.journalapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int NEW_JOURNAL_ACTIVITY_REQUEST_CODE = 1 ;
    private JournalViewModel mJournalViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mJournalViewModel = ViewModelProviders.of(this).get(JournalViewModel.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        mJournalViewModel.getAllWords().observe(this, new Observer<List<Journal>>() {
            @Override
            public void onChanged(@Nullable final List<Journal> words) {
                // Update the cached copy of the words in the adapter.


                //Adapter.setJournal(journal);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final JournalListAdapter adapter = new JournalListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Entity(tableName = "journal_table")
    public class Journal {
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "id")

        private int id;
        private String title;
        private String content;
        public Date published_on;

        public Journal(@NonNull String word)
        {this.id = id;
         this.title = title;
         this.content = content;
         this.published_on = published_on;
        }
        public int getId(){return this.id;}
        public String getTitle(){return this.title;}
        public String getContent(){return this.content;}
        public Date getPublished_on(){return this.published_on;}


    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_JOURNAL_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Journal word = new Journal(data.getStringExtra(NewJournalActivity.EXTRA_REPLY));
            mJournalViewModel.insert(word);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }



}





