package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalViewModel extends AndroidViewModel {
    private JournalRepository mRepository;

    private LiveData<List<MainActivity.Journal>> mAllWords;

    public JournalViewModel (Application application) {
        super(application);
        mRepository = new JournalRepository(application);
        LiveData<List<MainActivity.Journal>> mAllJournal = mRepository.getAllWords();
    }

    LiveData<List<MainActivity.Journal>> getAllWords() { return mAllWords; }

    public void insert(MainActivity.Journal journal) { mRepository.insert(journal); }

}
