package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalRepository {
    private JournalDao mWordDao;
    private LiveData<List<MainActivity.Journal>> mAllWords;

    JournalRepository(Application application) {
        JournalRoomDatabase db = JournalRoomDatabase.getDatabase(application);
        mWordDao = db.journalDao();
        mAllWords = mWordDao.getAllWords();
    }

    LiveData<List<MainActivity.Journal>> getAllWords() {
        return mAllWords;
    }

    public void insert (MainActivity.Journal word) {
        new insertAsyncTask(mWordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<MainActivity.Journal, Void, Void> {

        private JournalDao mAsyncTaskDao;

        insertAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MainActivity.Journal... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
