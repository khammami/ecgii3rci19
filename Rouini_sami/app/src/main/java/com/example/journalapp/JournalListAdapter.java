package com.example.journalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class JournalListAdapter extends RecyclerView.Adapter<JournalListAdapter.JournalViewHolder> {

    private final LayoutInflater mInflater;
    private List<Journal> mJournals; // Cached copy of words

    JournalListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public JournalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new JournalViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JournalViewHolder holder, int position) {
        if (mJournals != null) {
            Journal current = mJournals.get(position);
            holder.wordItemView.setText(current.getId());
        } else {
            // Covers the case of data not being ready yet.
            holder.wordItemView.setText("No Journal !");
        }
    }

    void setWords(List<Journal> words){
        mJournals = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mJournals != null)
            return mJournals.size();
        else return 0;
    }

    class JournalViewHolder extends RecyclerView.ViewHolder {
        private final TextView wordItemView;

        private JournalViewHolder(View itemView) {
            super(itemView);
            wordItemView = itemView.findViewById(R.id.textView);
        }
    }
}