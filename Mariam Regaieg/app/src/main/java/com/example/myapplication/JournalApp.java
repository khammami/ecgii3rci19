package com.example.myapplication;

import android.icu.text.CaseMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

public class JournalApp {   private String mWord;
    public JournalApp(@NonNull String Title) {this.mWord = Title;}

    public String getWord(){return this.mWord;}
    @Entity(tableName = "jornal_table")
    public class Word {

        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "title")

        private String mWord;

        public Word(@NonNull String Title) {this.mWord = CaseMap.Title;}

        public String getWord(){return this.mWord;}



    }
}

