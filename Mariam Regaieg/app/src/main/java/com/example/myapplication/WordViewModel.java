package com.example.myapplication;

import android.app.Application;
import android.icu.text.CaseMap;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WordViewModel extends AndroidViewModel {
    private WordRepository mRepository;

    private LiveData<List<CaseMap.Title>> mAllWords;

    public WordViewModel (Application application) {
        super(application);
        mRepository = new WordRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<CaseMap.Title>> getAllWords() { return mAllWords; }

    public void insert(CaseMap.Title word) { mRepository.insert(word); }
}
