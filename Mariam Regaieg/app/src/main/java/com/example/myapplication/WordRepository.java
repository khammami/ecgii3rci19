package com.example.myapplication;

import android.app.Application;
import android.icu.text.CaseMap;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WordRepository {
    private WordDao mWordDao;
    private LiveData<List<CaseMap.Title>> mAllWords;

    WordRepository(Application application) {
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        mWordDao = db.wordDao();
        mAllWords = mWordDao.getAllWords();
    }

    LiveData<List<CaseMap.Title>> getAllWords() {
        return mAllWords;
    }

    public void insert (CaseMap.Title word) {
        new insertAsyncTask(mWordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<CaseMap.Title, Void, Void> {

        private WordDao mAsyncTaskDao;

        insertAsyncTask(WordDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final CaseMap.Title... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
