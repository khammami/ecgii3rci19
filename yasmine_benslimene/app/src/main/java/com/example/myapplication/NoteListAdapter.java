package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {
    private final LayoutInflater mInflater;
    private List<Note> mNotes; // Cached copy of words

    NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        if (mNotes != null) {
            Note current = mNotes.get(position);

            holder.noteItemView1.setText(current.getmTitle());
            holder.noteItemView2.setText(current.getmContent());
            holder.noteItemView3.setText(current.getmPublished_on().toString());
        } else {
            // Covers the case of data not being ready yet.
            holder.noteItemView1.setText("No Note");
            holder.noteItemView2.setText("No Note");
            holder.noteItemView3.setText("No Note");

        }
    }

    void setWords(List<Note> notes){
        mNotes = notes;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mNotes != null)
            return mNotes.size();
        else return 0;
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {
        private final TextView noteItemView1;

        private final TextView noteItemView2;

        private final TextView noteItemView3;

        private NoteViewHolder(View itemView) {
            super(itemView);
            noteItemView1 = itemView.findViewById(R.id.textView1);
            noteItemView2 = itemView.findViewById(R.id.textView2);
            noteItemView3 = itemView.findViewById(R.id.textView3);

        }

    }
}
