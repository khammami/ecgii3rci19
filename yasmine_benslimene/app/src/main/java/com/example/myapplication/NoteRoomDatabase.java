package com.example.myapplication;

import android.content.Context;
import android.provider.ContactsContract;

import androidx.room.Database;
//import androidx.room.Room;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {ContactsContract.CommonDataKinds.Note.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class NoteRoomDatabase extends RoomDatabase {

public abstract NoteDao NoteDao();

    private static NoteRoomDatabase INSTANCE;

     public static NoteRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (NoteRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            NoteRoomDatabase.class, "note_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
        //INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                //NoteRoomDatabase.class, "note_database")
                //.build();
    }}

