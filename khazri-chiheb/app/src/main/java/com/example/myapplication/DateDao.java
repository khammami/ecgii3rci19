package com.example.myapplication;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DateDao {

    @Insert
    void insert(Date Date);

    @Query("DELETE FROM Date_table")
    void deleteAll();

    @Query("SELECT * from Date_table ORDER BY Date ASC")
    LiveData<List<Date>> getAllDates();
}