package com.example.myapplication;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DateListAdapter extends RecyclerView.Adapter<DateListAdapter.DateViewHolder> {

    private final LayoutInflater mInflater;
    private List<Date> mWords; // Cached copy of words

    DateListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public DateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new DateViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DateViewHolder holder, int position) {
        if (mDate != null) {
            Date current = mWords.get(position);
            holder.DateItemView.setText(current.getDate());
        } else {
            // Covers the case of data not being ready yet.
            holder.DateItemView.setText("No Word");
        }
    }

    void setWords(List<Date> words){
        mWords = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mWords != null)
            return mWords.size();
        else return 0;
    }

    class WordViewHolder extends RecyclerView.ViewHolder {
        private final TextView wordItemView;

        private WordViewHolder(View itemView) {
            super(itemView);
            wordItemView = itemView.findViewById(R.id.textView);
        }
    }

    public class DateViewHolder extends RecyclerView.ViewHolder {
    }
}