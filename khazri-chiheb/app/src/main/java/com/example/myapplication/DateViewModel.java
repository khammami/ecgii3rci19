package com.example.myapplication;


import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class DateViewModel extends AndroidViewModel {

    private DateRepository mRepository;

    private LiveData<List<Date>> mAllWords;

    public DateViewModel (Application application) {
        super(application);
        mRepository = new DateRepository(application);
        mAllDate = mRepository.getAllDate();
    }

    LiveData<List<Date>> getAllWords() { return mAllWords; }

    public void insert(Date word) { mRepository.insert(word); }
}