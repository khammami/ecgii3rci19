package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//deprecated
//use androidx instead

@Entity(tableName = "Date_table")
public class Date{
    @PrimaryKey()
    @NonNull
    @ColumnInfo(name = "Date")
    private String mDate;

    public Date(@NonNull String Date) {this.mDate = Date;}

    public String getDate(){return this.mDate;}
}