package com.example.myapplication;


import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class DateRepository {

    private DateDao mDateDao;
    private LiveData<List<Date>> mAllDate;

    DateRepository(Application application) {
        DateRoomDatabase db = DateRoomDatabase.getDatabase(application);
        mDateDao = db.DateDao();
        mAllDate = mDateDao.getAllDates();
    }

    LiveData<List<Date>> getAllDate() {
        return mAllDate;
    }

    public void insert (Date Date) {
        new insertAsyncTask(mDateDao).execute(Date);
    }

    private static class insertAsyncTask extends AsyncTask<Date, Void, Void> {

        private DateDao mAsyncTaskDao;

        insertAsyncTask(DateDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Date... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}