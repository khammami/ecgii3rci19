package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class DateViewModel extends AndroidViewModel {


    private DateRepository mRepository;

    private LiveData<List<Date1>> mAllDate;

    public DateViewModel(Application application) {
        super(application);
        mRepository = new DateRepository(application);
        mAllDate = mRepository.getAllDate();
    }

    LiveData<List<Date1>> getAllDate() { return mAllDate; }

    public void insert(Date1 date) { mRepository.insert(date); }


}
