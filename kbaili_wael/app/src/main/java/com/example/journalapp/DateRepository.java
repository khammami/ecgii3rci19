package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class DateRepository {

    private DateDao mDateDao;
    private LiveData<List<Date1>> mAllDate;

    DateRepository(Application application) {
        DateRoomDatabase db = DateRoomDatabase.getDatabase(application);
        mDateDao = db.dateDao();
        mAllDate = mDateDao.getAllDate();
    }

    LiveData<List<Date1>> getAllDate() {
        return mAllDate;
    }

    public void insert (Date1 date) {
        new insertAsyncTask(mDateDao).execute(date);
    }

    private static class insertAsyncTask extends AsyncTask<Date1, Void, Void> {

        private DateDao mAsyncTaskDao;

        insertAsyncTask(DateDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Date1... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
