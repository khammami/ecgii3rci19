package com.example.journalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DateListAdapter extends RecyclerView.Adapter<DateListAdapter.DateViewHolder> {

    private final LayoutInflater mInflater;
    private List<Date1> mDate; // Cached copy of words

    DateListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public DateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new DateViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DateViewHolder holder, int position) {
        if (mDate != null) {
            Date1 current = mDate.get(position);
            holder.dateItemView.setText(current.getId());
        } else {
            // Covers the case of data not being ready yet.
            holder.dateItemView.setText("No Word");
        }
    }

    void setWords(List<Date1> date) {
        mDate = date;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mDate != null)
            return mDate.size();
        else return 0;
    }

    class DateViewHolder extends RecyclerView.ViewHolder {
        private final TextView dateItemView;

        private DateViewHolder(View itemView) {
            super(itemView);
            dateItemView = itemView.findViewById(R.id.textView);
        }
    }
}

