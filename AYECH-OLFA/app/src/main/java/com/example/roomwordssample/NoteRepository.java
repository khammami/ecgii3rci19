package com.example.roomwordssample;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteRepository {
    private NoteDao noteDao;
    private LiveData<List<Note>> mAllNote;

    NoteRepository(Application application) {
       NoteRoomDatabase db = NoteRoomDatabase.getDatabase(application);
       noteDao = db.wordDao();
        mAllNote = NoteDao.getAllNote();
    }

    LiveData<List<Note>> getAllNote() {
        return mAllNote;
    }

    public void insert (Note note) {
        new insertAsyncTask(noteDao).execute(note);
    }


    private static class insertAsyncTask extends AsyncTask< Note, Void, Void> {

        private NoteDao mAsyncTaskDao;


        insertAsyncTask( NoteDao dao) {
            mAsyncTaskDao = dao;
        }
        public void deleteNote(Note note)  {
            new deleteNoteAsyncTask(noteDao).execute(note);
        }

        private static class deleteNoteAsyncTask extends AsyncTask<Note, Void, Void> {
            private NoteDao mAsyncTaskDao;

            deleteNoteAsyncTask(NoteDao dao) {
                mAsyncTaskDao = dao;
            }


            @Override
        protected Void doInBackground(final Note... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
            protected Void doInBackground(final Note... params) {
                mAsyncTaskDao.deleteAll(params[0]);
                return null;
            }
    }}

