package com.example.roomwordssample;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

public interface NoteDao {
    @Insert
    void insert(Note note);

    @Query("DELETE FROM note_table")
    void deleteAll();

    @Query("SELECT * from note_table ORDER BY   id ASC")
    static LiveData<List<Note>> getAllNote();
    @Delete
    void deleteNote(Note note);
}

