package com.example.roomwordssample;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "note_table")
public class Note {
    private Date published_on ;

    private String title;
    private String content;
    @PrimaryKey(autoGenerate=true)
    private int id;
       // @NonNull
        //@ColumnInfo(name = "title")


        public Note(@NonNull String title,@NonNull String content, @NonNull Date published_on,@NonNull int id)
    {this.title = title;
        this.content =content;
        this.published_on =published_on ;
        this.id=id;

    }

        public String getTitle(){return this.title;}
    public String getContent(){return this.content;}


    public int getId() {return id;}

    public void setId(int id) {
        this.id = id;
    }






}

