package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Query("DELETE FROM Note_table")
    void deleteAll();
    @Query("SELECT * from note_table ORDER BY note ASC")
    LiveData<List<Note>> getAllNotes();
}