package com.example.journalapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

public class Note {


    @Entity(tableName = "id_table")
    public class Id {

        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "id")
        private String mId;

        public Id(@NonNull String id) {
            this.mId = id;
        }

        public String getId() {
            return this.mId;
        }
    }

    @Entity(tableName = "id_table")
    public class Title {

        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "Title")
        private String mTitle;

        public Title(@NonNull String title) {
            this.mTitle = title;
        }

        public String getTitle() {
            return this.mTitle;
        }
    }

    public class Content {

        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "Content")
        private String mContent;

        public Content(@NonNull String content) {
            this.mContent = content;
        }

        public String getContent() {
            return this.mContent;
        }
    }
}